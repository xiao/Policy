import Foundation
import UIKit

public func colorFromDecimalRGB(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
    return UIColor(
        red: red / 255.0,
        green: green / 255.0,
        blue: blue / 255.0,
        alpha: alpha
    )
}

extension UIColor {
    
    class var brandPrimary: UIColor {
        return colorFromDecimalRGB(5, 55, 139)
    }
    
    class var brandSecondary: UIColor {
        return colorFromDecimalRGB(49, 180, 184)
    }
    
    class var brandSales: UIColor {
        return colorFromDecimalRGB(96, 165, 47)
    }
    
    class var grayDark: UIColor {
        return colorFromDecimalRGB(0, 0, 0)
    }
    
    class var grayCopy: UIColor {
        return colorFromDecimalRGB(65, 65, 65)
    }
    
    class var grayLight: UIColor {
        return colorFromDecimalRGB(153, 153, 153)
    }
    
    class var grayLigher: UIColor {
        return colorFromDecimalRGB(234, 232, 232)
    }
    
    class var grayLightest: UIColor {
        return colorFromDecimalRGB(247, 247, 247)
    }
}
