//
//  Helper.swift
//  Policy
//
//  Created by jin on 12/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

import Foundation


class Helper {

    static let API = "https://www.dropbox.com/s/r23f4oxq3qri8n3/policies.json?dl=1"
    
    static var IOSDateReader:DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static func compareDate(lhs: Date?, rhs:Date?) -> Bool {
        switch (lhs, rhs) {
        case (nil, nil):
            return false
        case (nil, _):
            return true
        case (_, nil):
            return false
        case (let ldate, let rdate):
            return ldate! < rdate!
        }
    }
}
