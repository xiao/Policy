import Foundation
import RxSwift

enum POLICYTYPE:String {
    case CTP = "CTP GreenSlip Insurance"
    case HOME = "Home Buildings Insurance"
    case CARCOMPREHENSIVE = "Comprehensive Car Insurance"
}

enum POLICYSTATUS:String {
    case current = "Current"
    case renewal = "Renewal"
}

struct Policy{

    let policyNumber: String
    let startDate: Date
    let renewalDate: Date
    let policyStatus: POLICYSTATUS
    let policyType: POLICYTYPE
    let policyDesc: String?
    let premium: Float?
    
    
    init?(json:[String: Any]) {
        guard  let policyNumber = json["policy_number"] as? String,
            let startDate = json["start_date"] as? String,
            let renewalDate = json["renewal_date"] as? String,
            let policyStatusRaw = json["policy_status"] as? String,
            let policytypeRaw = json["policy_type"] as? String,
            let policyStatus = POLICYSTATUS(rawValue:policyStatusRaw),
            let policytype = POLICYTYPE(rawValue:policytypeRaw)
        else {
            return nil
        }
        self.policyNumber = policyNumber
        self.startDate = Helper.IOSDateReader.date(from:startDate)!
        self.renewalDate = Helper.IOSDateReader.date(from:renewalDate)!
        self.policyStatus = policyStatus
        self.policyType = policytype
        self.policyDesc = json["description"] as? String
        self.premium = json["premium"] as? Float
    }
    
    
    var policyTypeIcon:String  {
        switch self.policyType {
        case .CTP:
            return "file"
        case .HOME:
            return "house"
        case .CARCOMPREHENSIVE:
            return "car"
        }
    }
    
}
