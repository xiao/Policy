import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let policies = Variable<[Policy]>([])
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My NRMA"
        addRrefreshButton()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PolicyDetailTableViewCell.self, forCellReuseIdentifier: PolicyDetailTableViewCellString)
        tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        
        
        
        policies.asObservable()
            .subscribe(onNext: {[weak self]
                 policies in
                for policy in policies {
                    if policy.policyStatus == POLICYSTATUS.renewal {
                        self?.showRenewalInfo(title: policy.policyType.rawValue, desc: policy.policyDesc)
                    }
                }
                
                DispatchQueue.main.async {
                    if policies.count > 0 {
                        SVProgressHUD.dismiss()
                    }
                    self?.tableView.reloadData()
                }
            })
            .disposed(by: bag)
        
        freshData()
       
    }
    
    private func addRrefreshButton() {
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        rightButton.setImage(UIImage(named:"mmc_wth_refresh")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate) , for: UIControlState.normal)
        rightButton.tintColor = UIColor.brandPrimary
        rightButton.rx.tap.subscribe(onNext:{
            [weak self]_ in
            self?.freshData()
        }).disposed(by: bag)
        
        let rightItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightItem
    }
    
    private func freshData(){
        SVProgressHUD.show()
        PolicyViewModel.getPolicyList()
            .timeout(30, scheduler: MainScheduler.instance)
            .catchError({ (error:Error) -> Observable<[Policy]> in
                DispatchQueue.main.async {
                    SVProgressHUD.showError(withStatus: "Failed to get the Data")
                    print("%@",error.localizedDescription)
                }
                return Observable.error(error)
            })
            .bind(to: policies).disposed(by: bag)
    }
    
    
    private func showRenewalInfo(title:String, desc:String?) {
        DispatchQueue.main.async {
            var message = ""
            if let desc = desc {
                message = String(format: "of %@", desc)
            }
            
            let alertAction = UIAlertController(title: "Important", message: String(format: "You need to renew %@ of %@", title, message), preferredStyle: .alert)
            let action = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: nil)
            alertAction.addAction(action)
            self.present(alertAction, animated: true, completion: nil);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return policies.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PolicyDetailTableViewCellString) as! PolicyDetailTableViewCell
        let policy = policies.value[indexPath.row]
        cell.configureCell(policy: policy)
        return cell;
    }
}

extension ViewController:UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let policy = policies.value[indexPath.row]
        if policy.policyStatus == .renewal {
            return 150
        }else {
            return 120
        }
    }
}
