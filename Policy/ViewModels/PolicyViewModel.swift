//
//  PolicyViewModel.swift
//  Policy
//
//  Created by jin on 12/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum PolicyError: Error {
    case invalidURL(String)
    case invalidParameter(String, Any)
    case invalidJSON(String)
    case serverFailed(String)
    
    func desc() -> String {
        switch self {
        case .invalidURL(let obj):
            return obj;
        case .invalidParameter(let obj, _):
            return "invalid Parameter".appendingFormat("%@", obj)
        case .invalidJSON(let obj):
            return "invalid JSON".appendingFormat("%@", obj)
        case .serverFailed(let obj):
            return "Network issue".appendingFormat("%@", obj)
        }
    }
}


struct PolicyViewModel {

    static let API = "https://www.dropbox.com/s/r23f4oxq3qri8n3/policies.json?dl=1"
    
    static func getPolicyList() -> Observable<[Policy]> {
        let policyURL = URL(string: API)!
        let request = URLRequest(url: policyURL)
        
        return URLSession.shared.rx.response(request:request)
            .catchError({ (error :Error) -> Observable<(HTTPURLResponse, Data)> in
                
                return Observable.error(PolicyError.serverFailed(""))
            })
            .map { response, date  in
                guard let jsonObject = try? JSONSerialization.jsonObject(with: date, options: []),
                    let objects = jsonObject as? Array<[String: Any]> else {
                        throw PolicyError.invalidJSON(API)
                }
                return objects.flatMap(Policy.init).sorted(by: { (obj1, obj2) -> Bool in
                    return Helper.compareDate(lhs: obj1.renewalDate, rhs: obj2.renewalDate)})
            }
            .share()
    }

}
