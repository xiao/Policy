import UIKit
let PolicyDetailTableViewCellString = "PolicyDetailTableViewCell"

class PolicyDetailTableViewCell: UITableViewCell {
    
    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 15
        stack.distribution = .fillProportionally
        return stack
    }()
    
    private var seperateLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        return view
    }()
    
    private  var  imageContentView : UIView = {
        let imageContentView = UIView()
        imageContentView.backgroundColor = UIColor.clear
        imageContentView.translatesAutoresizingMaskIntoConstraints = false
        return imageContentView
    }()
    
    private  var iconImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        return imageView
    }()
    
    private var detailstackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 0
        stack.distribution = .fillProportionally
        return stack
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Times New Roman", size: 16.0)
        label.textColor = UIColor.grayCopy
        label.numberOfLines = 1
        return label
    }()
    
    private var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Times New Roman", size: 14.0)
        label.textColor = UIColor.grayLight
        label.numberOfLines = 1
        return label
    }()
    
    private var typeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Times New Roman", size: 14.0)
        label.textColor = UIColor.grayLight
        label.numberOfLines = 1
        return label
    }()
    
    
    private var premiumLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Times New Roman", size: 14.0)
        label.textColor = UIColor.grayLight
        label.numberOfLines = 1
        return label
    }()
    
    
    private var errorStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 0
        stack.alignment = UIStackViewAlignment.center
        stack.distribution = .fill
        return stack
    }()
    
    private var renewAlertLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Times New Roman", size: 14.0)
        label.textColor = UIColor.red
        label.numberOfLines = 1
        label.text = "Please renew"
        return label
    }()
    
    private var alertImageView : UIImageView = {
        let alerView = UIImageView(image: UIImage(named:"alert"))
        alerView.translatesAutoresizingMaskIntoConstraints = false
        return alerView
    }()
    
    private var progressWrap:UIView =  {
        let progressWrap = UIView()
        progressWrap.translatesAutoresizingMaskIntoConstraints = false
        progressWrap.backgroundColor = UIColor.clear
        return progressWrap
    }()

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(stackView)
        self.contentView.addSubview(seperateLine)
        
        stackView.addArrangedSubview(imageContentView)
        imageContentView.addSubview(iconImageView)
        
        stackView.addArrangedSubview(detailstackView)
        detailstackView.addArrangedSubview(titleLabel)
        detailstackView.addArrangedSubview(dateLabel)
        detailstackView.addArrangedSubview(typeLabel)
        detailstackView.addArrangedSubview(premiumLabel)
        
        
        
        detailstackView.addArrangedSubview(errorStackView)
        errorStackView.addArrangedSubview(renewAlertLabel)
        errorStackView.setContentHuggingPriority(230, for: UILayoutConstraintAxis.vertical)
        progressWrap.addSubview(alertImageView)
        errorStackView.addArrangedSubview(progressWrap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        
        seperateLine.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        seperateLine.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
        seperateLine.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -10).isActive = true
        seperateLine.heightAnchor.constraint(equalToConstant: 1).isActive = true

        
        iconImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: imageContentView.centerYAnchor).isActive = true
        iconImageView.leftAnchor.constraint(equalTo: imageContentView.leftAnchor, constant: 14.0).isActive = true
        iconImageView.rightAnchor.constraint(equalTo: imageContentView.rightAnchor, constant: -14.0).isActive = true

        titleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        dateLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        typeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(policy:Policy) {
        if let desc = policy.policyDesc {
            self.titleLabel.text = desc
        } else {
            self.titleLabel.text = policy.policyType.rawValue
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        self.dateLabel.text = String(format: "%@ - %@", formatter.string(from: policy.startDate), formatter.string(from: policy.renewalDate))
        self.typeLabel.text = policy.policyType.rawValue
        
        if let amount = policy.premium {
            self.premiumLabel.text = String(format: "$%.2f", amount)
        }
        
        self.iconImageView.image = UIImage(named: policy.policyTypeIcon)
       
        if  policy.policyStatus == POLICYSTATUS.renewal {
            premiumLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
            alertImageView.widthAnchor.constraint(equalTo: alertImageView.heightAnchor).isActive = true
            alertImageView.topAnchor.constraint(equalTo: progressWrap.topAnchor, constant:0).isActive = true
            alertImageView.bottomAnchor.constraint(equalTo: progressWrap.bottomAnchor, constant:0).isActive = true
            alertImageView.leftAnchor.constraint(equalTo: progressWrap.leftAnchor, constant:10).isActive = true
        }else {
            detailstackView.removeArrangedSubview(errorStackView)
            errorStackView.isHidden = true
            premiumLabel.heightAnchor.constraint(equalToConstant: 20).isActive = false
        }
        
    }


}
