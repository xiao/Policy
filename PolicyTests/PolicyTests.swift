//
//  PolicyTests.swift
//  PolicyTests
//
//  Created by jin on 12/8/17.
//  Copyright © 2017 innovation. All rights reserved.
//

import XCTest
import Nimble
import RxSwift
import RxCocoa


@testable import Policy

class PolicyTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPolicyModel(){
        let dict =  ["policy_number": "MOT019396613",
                     "start_date": "2016-11-22",
                     "renewal_date": "2017-11-22",
                     "policy_status": "Current",
                     "policy_type": "Comprehensive Car Insurance",
                     "description": "2014 TOYOTA AURION ABC123",
                     "premium": Float(1225.79)] as [String : Any]
        
        let policy = Policy(json: dict)
        expect(policy?.policyNumber).to(equal("MOT019396613"))
        expect(policy?.startDate).to(equal(Helper.IOSDateReader.date(from:"2016-11-22")!))
        expect(policy?.renewalDate).to(equal(Helper.IOSDateReader.date(from:"2017-11-22")!))
        expect(policy?.policyStatus).to(equal(.current))
        expect(policy?.policyType).to(equal(POLICYTYPE.CARCOMPREHENSIVE))
        expect(policy?.policyDesc).to(equal("2014 TOYOTA AURION ABC123"))
        expect(policy?.premium).to(equal(1225.79))
        
        
    }
    
    func testNilPolicyModel(){
        let dict =  [
                     "start_date": "2016-11-22",
                     "renewal_date": "2017-11-22",
                     "policy_status": "Current",
                     "policy_type": "Comprehensive Car Insurance",
                     "description": "2014 TOYOTA AURION ABC123",
                     "premium": Float(1225.79)] as [String : Any]
        
        let policy = Policy(json: dict)
        expect(policy).to(beNil())
        
    }
    
    func testHelpFeature() {
        let future = Helper.IOSDateReader.date(from: "2017-12-11")
        
        var reuslt = Helper.compareDate(lhs: future, rhs: Date.init())
        expect(reuslt).to(beFalse())
        
        let past = Helper.IOSDateReader.date(from: "2017-01-11")
        reuslt = Helper.compareDate(lhs: past, rhs: Date.init())
        expect(reuslt).to(beTrue())

        reuslt = Helper.compareDate(lhs: nil, rhs: Date.init())
        expect(reuslt).to(beTrue())

        reuslt = Helper.compareDate(lhs: nil, rhs: nil)
        expect(reuslt).to(beFalse())
        
        reuslt = Helper.compareDate(lhs:  Date.init(), rhs: nil)
        expect(reuslt).to(beFalse())
        
    }
    
    
    func testUrlRequest() {
        let url = URL(string: "https://www.dropbox.com/s/r23f4oxq3qri8n3/policies.json?dl=1")
        let promise = expectation(description: "Status code: 200")
        
        let dataTask = URLSession.shared.dataTask(with: url!) { data, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    // 2
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testViewModel() {
        let asyncExpect = expectation(description: "fullfill test")
        
        let viewModel = PolicyViewModel.getPolicyList()
        
        let subscription = viewModel.subscribe(onNext:{ _ in
            asyncExpect.fulfill()
        })

        waitForExpectations(timeout: 2.0, handler: { error in
            XCTAssertNil(error, "error: \(error!.localizedDescription)")
            subscription.dispose()
        })
    }
    
    
}
