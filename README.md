# Policy Project

The explicit requirements are:
 - As a user(of the application) I can see the list of my policies
 - As a user I can get notfied when the policy expired
 - As a user I can fresh my policies


# Architecutre
The app is designed using MVVM 

The app used RxSwift, Rxcocoa framwork to make a clean architechture with stable, predictable and modular behavior

Scene: Refers to a screen managed by view controller. display the list of policy

View Model: Defines the data by the view controller to present a particular scene, the data fetched from Api

Model:the data used in the application


# Environment
Xcode 8
iphone simulator 10.1
